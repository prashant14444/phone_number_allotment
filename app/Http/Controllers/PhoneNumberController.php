<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PhoneNumber;
use App\Models\AllotedNumber;

class PhoneNumberController extends Controller
{
	private $retryCount = 0;
	public function getAvailableNumber(Request $request)
	{
		$pattern = intval($request->input('pattern'));
		$receivedPattern = $request->input('pattern');
		$responseMessage = 'Pattern should be in between 11111111111 and 9999999999';
		$status = 'success';
		$phoneNumber = null;
		if(!$this->validPattern($receivedPattern)){
			return ['message' => $responseMessage, 'satus' => 'error', 'data' => ['receivedPattern' => $receivedPattern]];
		}

		$responseMessage = 'Phone number alloted successfully.';
		$count = (new PhoneNumber)->count();
		if($count !== 0){
			if($pattern == 0){
				$phoneNumber = $this->getPhoneNumber();
			}
			else{
				$alreadyAlloted =  (new AllotedNumber)->hasNumber($receivedPattern);
				$randomlyAlloted = (new PhoneNumber)->checkAllocation($receivedPattern);
				if($alreadyAlloted || $randomlyAlloted){
					$responseMessage = 'Asked number is already in use. So we have alloted a different number.';
					$phoneNumber = $this->getPhoneNumber();
				}
				else{
					$phoneNumber = $receivedPattern;	
					$object = (new AllotedNumber)->firstOrNew(['number' => $phoneNumber]);
					$object->save();
				}
			}
		}

		if($phoneNumber == null){
			$responseMessage = "No more phone number is available. Please report to authorities.";
			$phoneNumber = '';
			$status = 'error';
		}
		return ['message' => $responseMessage, 'status' => $status , 'data' => ['phoneNumber' => $phoneNumber]];
	}

	public function validPattern($receivedPattern)
	{
		$isValid = true;
		if($receivedPattern == null)
			$isValid = true;

		else if(intval($receivedPattern) != $receivedPattern)
			$isValid = false;

		else if(strlen($receivedPattern) != 10)
			$isValid = false;
		else if(intval($receivedPattern) < 1111111111 || intval($receivedPattern) > 9999999999)
			$isValid = false;


		return $isValid;
	}

	public function populateContacts()
	{
		for($i = 1; $i <= 9; $i++){
			$object = (new PhoneNumber)->firstOrNew(['next_number' => $i . "111111111"]);
			$object->next_number = $i . "111111111";
			$object->first_digit = $i;
			if($i == 9)
				$object->last_number = 999999999;
			else
				$object->last_number = $i+1 . "111111110";
			$object->save();
		}
	}

	public function getPhoneNumber()
	{
		$phoneNumber = (new PhoneNumber)->getRandomPhoneNumber();
		$responseMessage = 'Phone number alloted successfully.';
		$status = 'success';
		if($phoneNumber == null){
			return $phoneNumber;
		}
		else if($phoneNumber == -1){
			$phoneNumber = $this->getPhoneNumber();
		}
		else {
			(new PhoneNumber)->updateNextPhoneNumber($phoneNumber);
			$alreadyAlloted = (new AllotedNumber)->hasNumber($phoneNumber);
			if($alreadyAlloted){
				$this->retryCount = $this->retryCount + 1;
				$phoneNumber = $this->getPhoneNumber();
			}
		}
		return $phoneNumber;
	}
}
