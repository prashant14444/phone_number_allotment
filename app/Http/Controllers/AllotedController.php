<?php

namespace App\Http\Controllers;

use App\alloted;
use Illuminate\Http\Request;

class AllotedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\alloted  $alloted
     * @return \Illuminate\Http\Response
     */
    public function show(alloted $alloted)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\alloted  $alloted
     * @return \Illuminate\Http\Response
     */
    public function edit(alloted $alloted)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\alloted  $alloted
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, alloted $alloted)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\alloted  $alloted
     * @return \Illuminate\Http\Response
     */
    public function destroy(alloted $alloted)
    {
        //
    }
}
