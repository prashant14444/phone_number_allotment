<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model
{
    protected $table = 'phone_numbers';
	public $timestamps = true;
	protected $primaryKey = 'id';
	protected $guarded = ['id'];

	public function getRandomPhoneNumber()
	{
		$phoneNumber = -1;
		$object = $this->where('exhausted', 0)->inRandomOrder()->get()->toArray();
		if(empty($object))
			return null;
		$phoneNumber = $object[0]['next_number'];
		if($phoneNumber == $object[0]['last_number']){
			$this->where('last_number', $phoneNumber)->where('next_number', $phoneNumber)->update(['exhausted' => 1]);
		}
		else
			$this->where('last_number', $phoneNumber)->where('next_number', $phoneNumber)->update(['next_number' => $phoneNumber+1]);
		return $phoneNumber;
	}

	public function updateNextPhoneNumber($phoneNumber)
	{
		$object = $this->firstOrNew(['next_number' => $phoneNumber]);
		if($phoneNumber == $object->last_number)
			$object->exhausted = 1;
		
		else if($phoneNumber == "9999999999")
			$object->exhausted = 1;
		
		else
			$object->next_number = $phoneNumber + 1;
		$object->save();
		return true;
	}

	public function checkAllocation($receivedPattern)
	{
		$firstDigit = substr($receivedPattern, 0, 1); 
		$nextNumber = $this->where('first_digit', $firstDigit-1)->pluck('next_number')->all()[0];
		return $receivedPattern < $nextNumber ? true : false;
	}
}
