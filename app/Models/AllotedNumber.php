<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AllotedNumber extends Model
{
     protected $table = 'alloted_numbers';
	public $timestamps = true;
	protected $primaryKey = 'id';
	protected $guarded = ['id'];

	public function hasNumber($number)
	{
		$count = $this->where('number', $number)->count();
		return ($count > 0) ? true : false;
	}
}
